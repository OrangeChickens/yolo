package com.example.tararezvani.crooterwatch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class PinActivity extends Activity {

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.round_activity_pin2);
//        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
//        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
//            @Override
//            public void onLayoutInflated(WatchViewStub stub) {
//                mTextView = (TextView) stub.findViewById(R.id.text);
//            }
//        });


//        final Button deleteButton = (Button) findViewById(R.id.button10);
//        deleteButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Intent i = new Intent(getBaseContext(), MainActivity.class);
//                startActivity(i);
//                //we call getBaseContext instead of 'this' since this is not in the main
//                //activity but rather in an onclick.
////                startService(i);
////                Toast.makeText(MainActivity.this, "Yo", Toast.LENGTH_SHORT).show();
//            }
//        });

        final ImageButton submitButton = (ImageButton) findViewById(R.id.button11);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), PinConfirmActivity.class);
                startActivity(i);
            }
        });
    }
}
