package com.example.tararezvani.crooterwatch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class RateActivity extends Activity {

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.round_activity_rate);
//        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
//        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
//            @Override
//            public void onLayoutInflated(WatchViewStub stub) {
//                mTextView = (TextView) stub.findViewById(R.id.text);
//            }
//        });

//        final Button backButton = (Button) findViewById(R.id.button5);
//        backButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Intent i = new Intent(getBaseContext(), RecieveActivity.class);
//                startActivity(i);
//                //we call getBaseContext instead of 'this' since this is not in the main
//                //activity but rather in an onclick.
////                startService(i);
//            }
//        });

        final ImageButton rateButton = (ImageButton) findViewById(R.id.button6);
        rateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), ConfirmRatingActivity.class);
                startActivity(i);
                //we call getBaseContext instead of 'this' since this is not in the main
                //activity but rather in an onclick.
//                startService(i);
            }
        });
    }
}
