package com.example.tararezvani.crooterwatch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity {

    private TextView mTextView;
    ImageButton sendButton;
    ImageButton receiveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.round_activity_main);
//        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
//        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
//            @Override
//            public void onLayoutInflated(WatchViewStub stub) {
//                mTextView = (TextView) stub.findViewById(R.id.text);
//            }
//        });

        sendButton = (ImageButton) findViewById(R.id.button);
        receiveButton = (ImageButton) findViewById(R.id.button2);
        sendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), PinActivity.class);
                Log.d("myTag", "SendButton");
                startActivity(i);
                //we call getBaseContext instead of 'this' since this is not in the main
                //activity but rather in an onclick.
//                startService(i);
//                Toast.makeText(MainActivity.this, "Yo", Toast.LENGTH_SHORT).show();
            }
        });

        receiveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), SendPinActivity.class);
                Log.d("myTag2", "RecieveButton");
                startActivity(i);
                //we call getBaseContext instead of 'this' since this is not in the main
                //activity but rather in an onclick.
//                startService(i);
//                Toast.makeText(MainActivity.this, "Yo", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
