package com.example.orangechickens.crooter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.orangechickens.crooter.util.SystemUiHider;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class FullscreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);


        final ImageButton submitButton = (ImageButton) findViewById(R.id.mainScreen);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), candidateActivity.class);
                startActivity(i);
            }
        });



//
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_fullscreen);
//        final ImageButton submitButton = (ImageButton) findViewById(R.id.resumeScreen);
//        submitButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Intent i = new Intent(getBaseContext(), profileActivity.class);
//                startActivity(i);
//            }
//        });
    }
}
