package com.example.orangechickens.crooter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.AppKeyPair;

//# Main activity file for mobile


public class MainActivity extends Activity {
    ImageButton profile;
    ImageButton linkedin;
    ImageButton candidate;
    ImageButton calendar;
    ImageButton resume;
    ImageButton email;
    final static private String APP_KEY = "rwei1mdboe4p1k8";
    final static private String APP_SECRET = "q8lnkt7aovsek73";
    private DropboxAPI<AndroidAuthSession> mDBApi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
        try {
            authenticate();
        } catch (DropboxException e) {
            e.printStackTrace();
        }
        final ImageButton submitButton = (ImageButton) findViewById(R.id.mainScreen);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), candidateActivity.class);
                startActivity(i);
            }
        });
    }

    //Dropbox authentication function and grabbing the shared resume
    private void authenticate() throws DropboxException {
        AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
        AndroidAuthSession session = new AndroidAuthSession(appKeys);
        mDBApi = new DropboxAPI<AndroidAuthSession>(session);
        mDBApi.getSession().startOAuth2Authentication(MainActivity.this);
        DropboxAPI.DropboxLink resume_link =  mDBApi.share("crooter_resume.pdf");
    }



    //Code to return to app after finishing authentication
    @Override
    protected void onResume() {
        super.onResume();

        if (mDBApi.getSession().authenticationSuccessful()) {
            try {
                // Required to complete auth, sets the access token on the session
                mDBApi.getSession().finishAuthentication();

                String accessToken = mDBApi.getSession().getOAuth2AccessToken();
            } catch (IllegalStateException e) {
                Log.i("DbAuthLog", "Error authenticating", e);
            }
        }
    }

}
