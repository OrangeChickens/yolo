package com.example.orangechickens.crooter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by OrangeChickens on 11/27/15.
 */
public class candidateActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.candidate);
        final ImageButton submitButton = (ImageButton) findViewById(R.id.candidateScreen);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), resumeDropActivity.class);
                startActivity(i);
            }
        });
    }
}
