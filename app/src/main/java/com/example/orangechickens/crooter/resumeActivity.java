package com.example.orangechickens.crooter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by OrangeChickens on 11/27/15.
 */
public class resumeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.email);
        final ImageButton submitButton = (ImageButton) findViewById(R.id.resumeScreen);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), profileActivity.class);
                startActivity(i);
            }
        });


    }
}
